﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace FoodComa.BLL
{
    /// <summary>
    /// Business logic of the program.
    /// </summary>
    public class BusinessLogic
    {
        /// <summary>
        /// Google map's API key.
        /// </summary>
        public string GoogleMapsKey { get; set; }

        /// <summary>
        /// Bing map's API key.
        /// </summary>
        public string BingMapsKey { get; set; }

        /// <summary>
        /// Constructor, inputs the keys for the different APIs.
        /// </summary>
        public BusinessLogic()
        {
            this.GoogleMapsKey = "AIzaSyC_DpDDZCQP0zzh4T7ilLfUl5pOr9CSBjQ";
            this.BingMapsKey = "AvTIu1Hhei341idoPfHjQu96w4jiiZ24G8_KtzIZooQiDrYVP_cW1syV5oLL0bQb";
        }

        /// <summary>
        /// Selects a restaurant dependant of the initial address.
        /// </summary>
        /// <param name="query">Contains the address and distance query information.</param>
        /// <param name="venueCount">Amount of venues.</param>
        /// <returns>Returns a random restaurant.</returns>
        public Venue GetRestaurant(FoodComa.BLL.Query query, Coordinates coordinates, ref int venueCount)
        {
            // Convert distance from miles into metres, to adhere to Google conditions.
            var metreDistance = (int) (query.Distance*1609.344);

            // Cap the distance to 10km, to adhere to Google conditions.
            if (metreDistance > 10000)
            {
                metreDistance = 10000;
            }

            var localVenues = this.GetLocalVenues(coordinates, metreDistance);

            if (localVenues.Count == 0 || localVenues == null)
            {
                return null;
            }

            if (localVenues.Count == 1)
            {
                return this.GetRestaurantDetails(localVenues.First());
            }

            var decisionMaker = new DecisionMaker();

            var decidedVenue = decisionMaker.Decide(localVenues);

            // Get more details for the venue.
            venueCount = localVenues.Count;
            return this.GetRestaurantDetails(decidedVenue);
        }

        /// <summary>
        /// Gets detailed restaurant data from google and formats it for the view.
        /// </summary>
        /// <param name="venue">Venue details.</param>
        /// <returns>Returns a class with data from google.</returns>
        private Venue GetRestaurantDetails(Restaurant venue)
        {
            // Send the request to google and get response.
            var response = MakeRequest(this.CreateReferenceRequest(venue.GoogleReference));
            var formattedOpeningTimes = new List<FormattedOpeningHours>();

            // Get the opening hours element.
            var hasOpeningHours = response.Descendants("result").Elements("opening_hours");

            // If the returned data has opening hours information.
            if (hasOpeningHours != null)
            {
                // Put the data into classes.
                var openingTimes = ExtractOpeningTimes(hasOpeningHours);

                // If there are opening times.
                if (openingTimes.Count > 0)
                {
                    // Format the data for the view.
                    formattedOpeningTimes = FormatOpeningTimes(openingTimes);
                }
            }

            var formattedVenue = response
                .Descendants("result")
                .Select(x =>
                        new Venue
                            {
                                Name =
                                    RetrieveValue(x, "name", "Unknown"),
                                Address =
                                    RetrieveValue(x, "vicinity", "") !=
                                    ""
                                        ? RetrieveValue(x, "vicinity", "")
                                        : RetrieveValue(x,
                                                        "formatted_address",
                                                        "No Address"),
                                Latitude =
                                    this.RetrieveValue(x,
                                                       new List<string>
                                                           {
                                                               "geometry",
                                                               "location",
                                                               "lat"
                                                           }, "0.0"),
                                Longitude =
                                    this.RetrieveValue(x,
                                                       new List<string>
                                                           {
                                                               "geometry",
                                                               "location",
                                                               "lng"
                                                           }, "0.0"),
                                Rating = RetrieveValue(x, "rating", ""),
                                OpenNow =
                                    this.RetrieveValue(x,
                                                       new List<string>
                                                           {
                                                               "opening_hours",
                                                               "open_now"
                                                           },
                                                       ""),
                                PostCode = PostCodeCheck(x, "Unknown"),
                                Price =
                                    Int32.Parse(RetrieveValue(x,
                                                              "price_level",
                                                              "0")),
                                PhoneNumber =
                                    RetrieveValue(x,
                                                  "international_phone_number",
                                                  "") != ""
                                        ? RetrieveValue(x,
                                                        "international_phone_number",
                                                        "")
                                              .Replace("+44 ", "0")
                                        : RetrieveValue(x,
                                                        "formatted_phone_number",
                                                        ""),
                                Website = RetrieveValue(x, "website", ""),
                                OpeningTimes = formattedOpeningTimes,
                                Reviews = this.GetRestaurantReviews(x),
                                Photo = this.GetPhotos(x)
                            });

            return formattedVenue.First();
        }

        /// <summary>
        /// Gets the photos reference and details to be used later for image retrieval.
        /// </summary>
        /// <param name="response">XML document that contains information on the venue.</param>
        /// <returns>Returns an image returns>
        private Image GetPhotos(XElement response)
        {
            var responsePhotos = response.Elements("photo");

            var photos =  responsePhotos
                .Select(x =>
                        new Photo
                            {
                                PhotoReference = RetrieveValue(x, "photo_reference", ""),
                                Height = RetrieveValue(x, "height", ""),
                                Width = RetrieveValue(x, "width", ""),
                            }).ToList();

            return photos.Count > 0 ? MakeImageRequest(this.CreatePhotoRequest(photos.First())) : null;
        }

        /// <summary>
        /// Gets the information of the reviews of the restaurant.
        /// </summary>
        /// <param name="response">XML document containing information about the restaurant.</param>
        /// <returns>Returns a list of restaurant reviews.</returns>
        private List<Review> GetRestaurantReviews(XElement response)
        {
            // Extract the reviews.
            var reviews = response.Elements("review");

            var formattedReviews = new List<Review>();

            var textInfo = new CultureInfo("en-GB", false).TextInfo;

            formattedReviews
                .AddRange(reviews
                              .Select(x =>
                                      new Review(
                                          textInfo.ToTitleCase(RetrieveValue(x, "author_name",
                                                                             "Anonymous")),
                                          RetrieveValue(x, "text", ""),
                                          RetrieveValue(x, "rating", "")
                                          )));

            return formattedReviews;
        }

        /// <summary>
        /// Checks to see if a post code for the venue is available.
        /// </summary>
        /// <param name="response">XML data for the venue.</param>
        /// <param name="failValue">Fail string to return on null.</param>
        /// <returns></returns>
        private static string PostCodeCheck(XElement response, string failValue)
        {
            // Get the address_components where the "type" element isnt null.
            var retrievedTypes = response.Elements("address_component").Where(x => x.Element("type") != null);

            foreach (var element in response.Elements("address_component"))
            {
                foreach (var subElement in element.Elements("type"))
                {
                    if (subElement.Value == "postal_code")
                    {
                        return RetrieveValue(element, "long_name", failValue);
                    }
                }
            }

            return failValue;
        }

        /// <summary>
        /// Does a simply null-check and returns the value inside the element, if not it returns the fail value string.
        /// </summary>
        /// <param name="response">Response element in XML form.</param>
        /// <param name="elementName">Name being null-checked.</param>
        /// <param name="failValue">Fail string value to return if not found.</param>
        /// <returns></returns>
        private static string RetrieveValue(XElement response, string elementName, string failValue)
        {
            return response.Element(elementName) != null ? response.Element(elementName).Value : failValue;
        }

        /// <summary>
        /// A recursive XML element null checker that goes down the elementNames to check if a value exists.
        /// </summary>
        /// <param name="response">XML XElement data.</param>
        /// <param name="elementNames">List of element names, in hierarchical order.</param>
        /// <param name="failValue">String to return on null.</param>
        /// <returns></returns>
        private string RetrieveValue(XElement response, ICollection<string> elementNames, string failValue)
        {
            if (elementNames.Count() == 1)
            {
                // Do null check and value retrieval.
                return RetrieveValue(response, elementNames.First(), failValue);
            }
            else
            {
                if (response.Element(elementNames.First()) != null)
                {
                    var retrievedData = response.Element(elementNames.First());
                    // Remove the first element name from the collection of element names.
                    elementNames.Remove(elementNames.First());

                    // Update the response XElement with only the important information.
                    return this.RetrieveValue(retrievedData, elementNames, failValue);
                }
                else
                {
                    return failValue;
                }
            }
        }

        /// <summary>
        /// Extracts the data from XML for opening times.
        /// </summary>
        /// <param name="hasOpeningHours">Opening hours data.</param>
        /// <returns>Returns a list of opening times.</returns>
        private List<OpeningTime> ExtractOpeningTimes(IEnumerable<XElement> hasOpeningHours)
        {
            return hasOpeningHours
                .Elements("period")
                .Select(x =>
                        new OpeningTime
                            {
                                Position = x.Element("open").Element("day").Value,
                                ClosesAt =
                                    x.Element("close") != null
                                        ? x.Element("close")
                                           .Element("time")
                                           .Value.Substring(0, 2) + ":" +
                                          x.Element("close")
                                           .Element("time")
                                           .Value.Substring(2, 2)
                                        : "",
                                OpensAt =
                                    x.Element("open")
                                     .Element("time")
                                     .Value.Substring(0, 2) + ":" +
                                    x.Element("open")
                                     .Element("time")
                                     .Value.Substring(2, 2),
                            }).ToList();
        }

        /// <summary>
        /// Formats the opening times for display to the view.
        /// </summary>
        /// <param name="openingTimes">List of opening times of the venue.</param>
        /// <returns>Returns a formatted list of FormattedOpeningHours type.</returns>
        private List<FormattedOpeningHours> FormatOpeningTimes(List<OpeningTime> openingTimes)
        {
            // Format the opening times.
            // Sometimes restaurants open for lunch, close and then open for dinner service.
            var groupedDays = openingTimes.GroupBy(x => x.Position).ToList();
            var formattedTimes = new List<FormattedOpeningHours>();

            var daysOfTheWeek = new string[]
                {"Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"};

            foreach (var day in groupedDays)
            {
                if (day.Count() > 1)
                {
                    var hours = day.ToList();

                    formattedTimes.Add(
                        new FormattedOpeningHours
                            {
                                Day = daysOfTheWeek[int.Parse(day.ToList()[0].Position)],
                                LunchHours = string.Format("{0} - {1}", hours[0].OpensAt, hours[0].ClosesAt),
                                DinnerHours = string.Format("{0} - {1}", hours[1].OpensAt, hours[1].ClosesAt)
                            });
                }
                else if (day.Count() == 1)
                {
                    formattedTimes.Add(
                        new FormattedOpeningHours
                            {
                                Day = daysOfTheWeek[int.Parse(day.ToList()[0].Position)],
                                LunchHours = string.Format("{0} - {1}", day.First().OpensAt, day.First().ClosesAt),
                                DinnerHours = null
                            });
                }
            }

            // Get all the days that don't have opening hours.
            for (int i = 0; i < 7; i++)
            {
                if (!formattedTimes.Any(x => x.Day == daysOfTheWeek[i]))
                {
                    formattedTimes.Add(
                        new FormattedOpeningHours
                            {
                                Day = daysOfTheWeek[i],
                                LunchHours = "Closed",
                                DinnerHours = null
                            });
                }
            }

            // The list comes in formatted as Sunday - Saturday,
            // move sunday to the end of the list so that it's Monday - Sunday.

            return formattedTimes;
        }

        /// <summary>
        /// Gets a list of local venues from google maps.
        /// </summary>
        /// <param name="coordinates">Latitude and longitude geocoordinates of the address.</param>
        /// <returns>Returns a list of local venues.</returns>
        private List<Restaurant> GetLocalVenues(Coordinates coordinates, int distance)
        {
            // Build the request URI.
            XDocument xml = MakeRequest(CreatePlacesRequest(coordinates, distance));

            return
                xml.Descendants("result")
                   .Where(y => y
                                   .Elements("type")
                                   .Any(z => z.Value == "restaurant"))
                   .Select(x =>
                           new Restaurant
                               {
                                   Name = RetrieveValue(x, "name", ""),
                                   GoogleReference = RetrieveValue(x, "reference", "")
                               })
                   .ToList();
        }

        /// <summary>
        /// Gets the geocoordinates of an address.
        /// </summary>
        /// <param name="address">Address specified by the user.</param>
        /// <returns>Returns the latitude and logitude of the address.</returns>
        public Coordinates GetCoordinates(string address)
        {
            // Remove spaces from the address.
            XDocument xml = MakeRequest(CreateGoogleGeoLocationRequest(address));

            // Extract the data from the xml file.
            // TODO: Extract the correct data from the XML file.
            var geography = xml.Descendants("geometry").Select(x => x.Element("location"));


            return new Coordinates(geography.Descendants("lng").First().Value,
                                   geography.Descendants("lat").First().Value);
        }


        /// <summary>
        /// Removes whitespace from the string.
        /// </summary>
        /// <param name="address">Address that comes in.</param>
        /// <returns>Returns a string with the whitespace removed.</returns>
        private string RemoveSpaces(string address)
        {
            return address.Replace(" ", "");
        }

        /// <summary>
        /// Gets the venues from google maps.
        /// </summary>
        /// <param name="requestUrl">Request url.</param>
        /// <returns>Returns unformatted XML.</returns>
        public static XDocument MakeRequest(string requestUrl)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                request.Proxy = WebRequest.DefaultWebProxy;
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;
                request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                return XDocument.Load(response.GetResponseStream());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                Console.Read();
                return null;
            }
        }

        /// <summary>
        /// Gets an image from a web request stream.
        /// </summary>
        /// <param name="requestUrl">Request URL to get the image from.</param>
        /// <returns>Returns an image.</returns>
        public static Image MakeImageRequest(string requestUrl)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                request.Proxy = WebRequest.DefaultWebProxy;
                request.Credentials = System.Net.CredentialCache.DefaultCredentials;
                request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                var stream = request.GetResponse().GetResponseStream();

                return Image.FromStream(stream);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                Console.Read();
                return null;
            }
        }

        /// <summary>
        /// Creates a request url for google maps.
        /// </summary>
        /// <param name="coordinates">Geocoordinates of the location.</param>
        /// <returns>Returns a formatted string for searching the local area for restaurants.</returns>
        private string CreatePlacesRequest(Coordinates coordinates, int distance)
        {
            return
                string.Format(
                    "https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location={0},{1}&radius={2}&types=food&sensor=false&key={3}",
                    coordinates.Latitude,
                    coordinates.Longitude,
                    distance,
                    this.GoogleMapsKey);
        }

        /// <summary>
        /// Creates a request url for google references.
        /// </summary>
        /// <param name="googleReference">Reference to query.</param>
        /// <returns>Returns a formatted string for getting details of a venue.</returns>
        private string CreateReferenceRequest(string googleReference)
        {
            // TODO: Sensor, is whether it is from a geolocator.

            return
                string.Format(
                    "https://maps.googleapis.com/maps/api/place/details/xml?reference={0}&sensor=false&key={1}",
                    googleReference, this.GoogleMapsKey);
        }

        /// <summary>
        /// Creates a string that will retrieve the longitude and latitude from a postcode.
        /// </summary>
        /// <param name="postCode">Post code to look up.</param>
        /// <returns>Returns a formatted string.</returns>
        private string CreateLatitudeLongitudeRequest(string postCode)
        {
            return string.Format("http://uk-postcodes.com/postcode/{0}.xml", postCode);
        }

        /// <summary>
        /// Creates the string to send a request to the Google servers for a GeoLocation lookup.
        /// </summary>
        /// <param name="address">Address to get Long/Lat for.</param>
        /// <returns>Returns the request string.</returns>
        private string CreateGoogleGeoLocationRequest(string address)
        {
            var formattedAddress = address.Replace("%20", "+");
            formattedAddress = formattedAddress.Replace(" ", "+");
            return string.Format("https://maps.googleapis.com/maps/api/geocode/xml?address={0}&key={1}",
                                 formattedAddress, this.GoogleMapsKey);
        }

        /// <summary>
        /// Creates the string to send a request to the Google servers for the photo.
        /// </summary>
        /// <param name="photo">Photo details.</param>
        /// <returns></returns>
        private string CreatePhotoRequest(Photo photo)
        {
            return
                string.Format(
                    "https://maps.googleapis.com/maps/api/place/photo?maxwidth={0}&photoreference={1}&sensor=false&key={2}",
                    photo.Width,
                    photo.PhotoReference,
                    this.GoogleMapsKey);
        }
    }
}