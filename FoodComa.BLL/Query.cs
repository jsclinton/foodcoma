﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    public class Query
    {
        public string QueryAddress { get; set; }
        public double Distance { get; set; }
        public bool UseGps { get; set; }
        public Coordinates Coordinates { get; set; }

        public Query(string address, double distance, bool useGps, Coordinates coordinates)
        {
            this.QueryAddress = address;
            this.Distance = distance;
            this.UseGps = useGps;
            this.Coordinates = coordinates;
        }
    }
}