﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    public class Review
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Rating { get; set; }

        public Review()
        {
            
        }

        public Review(string name, string text, string rating)
        {
            this.Name = name;
            this.Text = text;
            this.Rating = rating;
        }
    }
}