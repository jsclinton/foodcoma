﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    public class Venue
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string PostCode { get; set; }
        public string Rating { get; set; }
        public string Longitude { get; set; }
        public string Latitude { get; set; }
        public string OpenNow { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public int Price { get; set; }
        public List<FormattedOpeningHours> OpeningTimes { get; set; }
        public List<Review> Reviews { get; set; }
        public Image Photo { get; set; } 
    }
}