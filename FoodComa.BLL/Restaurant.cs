﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    public class Restaurant
    {
        public string Name { get; set; }
        public string GoogleReference { get; set; }
    }
}