﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    public class Photo
    {
        public string PhotoReference { get; set; }
        public string Height { get; set; }
        public string Width { get; set; }
    }
}