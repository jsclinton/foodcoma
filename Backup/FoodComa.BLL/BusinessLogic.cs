﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net;
using System.Xml;
using System.Xml.Linq;

namespace FoodComa.BLL
{
    /// <summary>
    /// Business logic of the program.
    /// </summary>
    public class BusinessLogic
    {
        /// <summary>
        /// Google map's API key.
        /// </summary>
        public string GoogleMapsKey { get; set; }

        /// <summary>
        /// Bing map's API key.
        /// </summary>
        public string BingMapsKey { get; set; }

        /// <summary>
        /// Constructor, inputs the keys for the different APIs.
        /// </summary>
        public BusinessLogic()
        {
            this.GoogleMapsKey = "AIzaSyC_DpDDZCQP0zzh4T7ilLfUl5pOr9CSBjQ";
            this.BingMapsKey = "AvTIu1Hhei341idoPfHjQu96w4jiiZ24G8_KtzIZooQiDrYVP_cW1syV5oLL0bQb";
        }

        /// <summary>
        /// Selects a restaurant dependant of the initial address.
        /// </summary>
        /// <param name="address">Address to get a restaurant from.</param>
        /// <returns>Returns a random restaurant.</returns>
        public Venue GetRestaurant(string address, int distance)
        {
            var geocoordinates = GetCoordinates(address);
            var localVenues = this.GetLocalVenues(geocoordinates, distance);

            var decisionMaker = new DecisionMaker();

            var decidedVenue = decisionMaker.Decide(localVenues);

            // Get more details for the venue.
            return this.GetRestaurantDetails(decidedVenue);
        }

        /// <summary>
        /// Gets detailed restaurant data from google and formats it for the view.
        /// </summary>
        /// <param name="venue">Venue details.</param>
        /// <returns>Returns a class with data from google.</returns>
        private Venue GetRestaurantDetails(Restaurant venue)
        {
            // Send the request to google and get response.
            var response = MakeRequest(this.CreateReferenceRequest(venue.GoogleReference));
            var formattedOpeningTimes = new List<FormattedOpeningHours>();

            // Get the opening hours element.
            var hasOpeningHours = response.Descendants("result").Elements("opening_hours");

            // If the returned data has opening hours information.
            if (hasOpeningHours != null)
            {
                // Put the data into classes.
                var openingTimes = ExtractOpeningTimes(hasOpeningHours);

                // If there are opening times.
                if (openingTimes.Count > 0)
                {
                    // Format the data for the view.
                    formattedOpeningTimes = FormatOpeningTimes(openingTimes);
                }
            }

            var formattedVenue = response.Descendants("result").Select(x =>
                new Venue
                {
                    Name = x.Element("name").Value,
                    Address = x.Element("vicinity") != null ? x.Element("vicinity").Value : (x.Element("formatted_address") != null ? x.Element("formatted_address").Value : "No Address"),
                    Latitude = x.Element("geometry").Element("location").Element("lat").Value,
                    Longitude = x.Element("geometry").Element("location").Element("lng").Value,
                    Rating = x.Element("rating") != null ? x.Element("rating").Value : "N/A",
                    OpenNow = x.Element("opening_hours") != null ? x.Element("opening_hours").Element("open_now").Value : "",
                    PostCode = x.Elements("address_component").First(y => (y.Element("type") != null) && (y.Element("type").Value == "postal_code")).Element("long_name").Value,
                    PhoneNumber = x.Element("international_phone_number") != null ? x.Element("international_phone_number").Value : "",
                    Website = x.Element("website") != null ? x.Element("website").Value : "",
                    OpeningTimes = formattedOpeningTimes
                });

            return formattedVenue.First();
        }

        /// <summary>
        /// Extracts the data from XML for opening times.
        /// </summary>
        /// <param name="hasOpeningHours">Opening hours data.</param>
        /// <returns>Returns a list of opening times.</returns>
        private List<OpeningTime> ExtractOpeningTimes(IEnumerable<XElement> hasOpeningHours)
        {
            return hasOpeningHours.Elements("period").Select(x => 
                new OpeningTime
                    {
                        Position = x.Element("open").Element("day").Value,
                        ClosesAt = x.Element("close").Element("time").Value.Substring(0, 2) + ":" + x.Element("close").Element("time").Value.Substring(2, 2),
                        OpensAt = x.Element("open").Element("time").Value.Substring(0, 2) + ":" + x.Element("open").Element("time").Value.Substring(2, 2),
                    }).ToList();
        }

        /// <summary>
        /// Formats the opening times for display to the view.
        /// </summary>
        /// <param name="openingTimes">List of opening times of the venue.</param>
        /// <returns>Returns a formatted list of FormattedOpeningHours type.</returns>
        private List<FormattedOpeningHours> FormatOpeningTimes(List<OpeningTime> openingTimes)
        {
            // Format the opening times.
            // Sometimes restaurants open for lunch, close and then open for dinner service.
            var groupedDays = openingTimes.GroupBy(x => x.Position).ToList();
            var formattedTimes = new List<FormattedOpeningHours>();

            var daysOfTheWeek = new string[]{ "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday" };

            foreach (var day in groupedDays)
	        {
                if (day.Count() > 1)
                {
                    var hours = day.ToList();

                    formattedTimes.Add(
                        new FormattedOpeningHours
                        {
                            Day = daysOfTheWeek[int.Parse(day.ToList()[0].Position)],
                            LunchHours = string.Format("{0} - {1}", hours[0].OpensAt, hours[0].ClosesAt),
                            DinnerHours = string.Format("{0} - {1}", hours[1].OpensAt, hours[1].ClosesAt)
                        });
                }
                else if(day.Count() == 1)
                {
                    formattedTimes.Add(
                        new FormattedOpeningHours
                        {
                            Day = daysOfTheWeek[int.Parse(day.ToList()[0].Position)],
                            LunchHours = string.Format("{0} - {1}", day.First().OpensAt, day.First().ClosesAt),
                            DinnerHours = null
                        });
                }
	        }

            // Get all the days that don't have opening hours.
            for (int i = 0; i < 7; i++)
            {
                if (!formattedTimes.Any(x => x.Day == daysOfTheWeek[i]))
                {
                    formattedTimes.Add(
                        new FormattedOpeningHours
                        {
                            Day = daysOfTheWeek[i],
                            LunchHours = "Closed",
                            DinnerHours = null
                        });
                }
            }

            // The list comes in formatted as Sunday - Saturday,
            // move sunday to the end of the list so that it's Monday - Sunday.
            
            return formattedTimes;
        }

        /// <summary>
        /// Gets a list of local venues from google maps.
        /// </summary>
        /// <param name="coordinates">Latitude and longitude geocoordinates of the address.</param>
        /// <returns>Returns a list of local venues.</returns>
        private List<Restaurant> GetLocalVenues(Coordinates coordinates, int distance)
        {
            // Build the request URI.
            XDocument xml = MakeRequest(CreatePlacesRequest(coordinates, distance));

            return xml.Descendants("result").Where(y => y.Elements("type").Any(z => z.Value == "restaurant")).Select(x =>
                new Restaurant
                {
                    Name = x.Element("name").Value,
                    GoogleReference = x.Element("reference").Value
                }).ToList();
        }

        /// <summary>
        /// Gets the geocoordinates of an address.
        /// </summary>
        /// <param name="address">Address specified by the user.</param>
        /// <returns>Returns the latitude and logitude of the address.</returns>
        private Coordinates GetCoordinates(string address)
        {
            // Remove spaces from the address.
            XDocument xml = MakeRequest(CreateLatitudeLongitudeRequest(this.RemoveSpaces(address)));

            // Extract the data from the xml file.
            var geography = xml.Descendants("result").Select(x => x.Element("geo"));

            return new Coordinates
            {
                Latitude = geography.First().Element("lat").Value,
                Longitude = geography.First().Element("lng").Value
            };
        }


        /// <summary>
        /// Removes whitespace from the string.
        /// </summary>
        /// <param name="address">Address that comes in.</param>
        /// <returns>Returns a string with the whitespace removed.</returns>
        private string RemoveSpaces(string address)
        {
            return address.Replace(" ", "");
        }

        /// <summary>
        /// Gets the venues from google maps.
        /// </summary>
        /// <param name="requestUrl">Request url.</param>
        /// <returns>Returns unformatted XML.</returns>
        public static XDocument MakeRequest(string requestUrl)
        {
            try
            {
                HttpWebRequest request = WebRequest.Create(requestUrl) as HttpWebRequest;
                request.Proxy = WebRequest.DefaultWebProxy;
                request.Credentials = System.Net.CredentialCache.DefaultCredentials; ;
                request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;

                HttpWebResponse response = request.GetResponse() as HttpWebResponse;

                return XDocument.Load(response.GetResponseStream());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                Console.Read();
                return null;
            }
        }

        /// <summary>
        /// Creates a request url for google maps.
        /// </summary>
        /// <param name="coordinates">Geocoordinates of the location.</param>
        /// <returns>Returns a formatted string for searching the local area for restaurants.</returns>
        private string CreatePlacesRequest(Coordinates coordinates, int distance)
        {
            return string.Format("https://maps.googleapis.com/maps/api/place/nearbysearch/xml?location={0},{1}&radius={2}&types=food&sensor=false&key={3}", 
                coordinates.Latitude,
                coordinates.Longitude,
                distance,
                this.GoogleMapsKey);
        }

        /// <summary>
        /// Creates a request url for google references.
        /// </summary>
        /// <param name="googleReference">Reference to query.</param>
        /// <returns>Returns a formatted string for getting details of a venue.</returns>
        private string CreateReferenceRequest(string googleReference)
        {
            // TODO: Sensor, is whether it is from a geolocator.

            return string.Format("https://maps.googleapis.com/maps/api/place/details/xml?reference={0}&sensor=false&key={1}", googleReference, this.GoogleMapsKey);
        }

        /// <summary>
        /// Creates a string that will retrieve the longitude and latitude from a postcode.
        /// </summary>
        /// <param name="postCode">Post code to look up.</param>
        /// <returns>Returns a formatted string.</returns>
        private string CreateLatitudeLongitudeRequest(string postCode)
        {
            return string.Format("http://uk-postcodes.com/postcode/{0}.xml", postCode);
        }
    }
}