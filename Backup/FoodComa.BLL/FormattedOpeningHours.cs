﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    public class FormattedOpeningHours
    {
        public string Day { get; set; }
        public string LunchHours { get; set; }
        public string DinnerHours { get; set; }
    }
}