﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    /// <summary>
    /// This class makes decisions.
    /// </summary>
    public class DecisionMaker
    {
        /// <summary>
        /// Picks a venue from a list of venues.
        /// </summary>
        /// <param name="venues">List of venues to choose from.</param>
        /// <returns>Returns one of the venues randomly.</returns>
        public Restaurant Decide(List<Restaurant> venues)
        {
            var randomisedVenues = this.RandomShuffle(venues);

            var random = new Random(DateTime.Now.Millisecond);
            var venueNumber = random.Next(0, randomisedVenues.Count);

            return randomisedVenues[venueNumber];
        }

        /// <summary>
        /// Shuffles the list of venues.
        /// </summary>
        /// <param name="venues">List of venues.</param>
        /// <returns>Returns a randomised list of venues.</returns>
        private List<Restaurant> RandomShuffle(List<Restaurant> venues)
        {
            var rng = new Random();
            int n = venues.Count;        // The number of items left to shuffle (loop invariant).
            while (n > 1)
            {
                int k = rng.Next(n);  // 0 <= k < n.
                n--;                     // n is now the last pertinent index;
                Restaurant temp = venues[n];     // swap array[n] with array[k] (does nothing if k == n).
                venues[n] = venues[k];
                venues[k] = temp;
            }

            return venues;
        }
    }
}