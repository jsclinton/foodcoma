﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    public class OpeningTime
    {
        public string Position { get; set; }
        public string OpensAt { get; set; }
        public string ClosesAt { get; set; }
    }
}