﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.BLL
{
    /// <summary>
    /// Coordinates class, stores latitude and longitude.
    /// </summary>
    public class Coordinates
    {
        /// <summary>
        /// Latitude string, North/South.
        /// </summary>
        public string Latitude { get; set; }

        /// <summary>
        /// Longitude string, East/West.
        /// </summary>
        public string Longitude { get; set; }
    }
}