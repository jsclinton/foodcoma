﻿$(document).ready(function () {

    $("#error-container").addClass("invisible")

    validateAddress($("#address"));
    validateDistance($("#distance"));

    // Force uppercase.
    $("#address").keyup(function () {
        $(this).val($(this).val().toUpperCase());
    });

    // Validate the postcode.
    $("#address").focusout(function () {
        validateAddress(this);
        addOrRemoveErrorClass(showAddressError, "address-container");
        showErrorMessage(this);
    });

    // Validate the distance.
    $("#distance").keyup(function () {
        validateDistance(this);
        addOrRemoveErrorClass(showDistanceError, "distance-container");
        showErrorMessage();
    });

    showErrorMessage();
});

// Three states are needed for this to work,
// 2 = undefined,
// 1 = true,
// 0 = false
var showAddressError = 2;
var showDistanceError = 2;

function validateAddress(value) {
    var input = $(value).val($(value).val());

    if (isValidPostcode(input.val()) == false && input.val().length > 0) {
        $("#postcode-error").html("<i class=\"icon-exclamation-sign\"></i> Please enter in a valid postcode<br />");
        showAddressError = 1;
    }
    else if (input.val().length == 0) {
        $("#postcode-error").html("");
        showAddressError = 2;
    } else {
        $("#postcode-error").html("");
        input.val(formatPostcode(input.val()));
        showAddressError = 0;
    }
}

function validateDistance(value) {
    if ($(value).val($(value).val()).val() > 50000) {
        // Max distance is 50,000m
        $("#distance-error").html("<i class=\"icon-exclamation-sign\"></i> Distance cannot be greater than 50,000m");
        showDistanceError = 1;
    } else if ($(value).val($(value).val()).val().length == 0) {
        $("#distance-error").html("");
        showDistanceError = 2;
    } else {
        $("#distance-error").html("");
        showDistanceError = 0;
    }
}

// Adds or removes the error class.
function addOrRemoveErrorClass(bool, container) {
    if (bool == 1) {
        $("#" + container).addClass("error");
    }
    else {
        $("#" + container).removeClass("error");
    }
}

function showErrorMessage() {
    if (showAddressError == 1 || showDistanceError == 1) {
        // Any errors.
        // Show the error messages.
        $("#error-container").removeClass("invisible");

        // Disable the submit button.
        $("#submit-button").attr("disabled", "disabled");

    } else if ((showAddressError == 0 && showDistanceError == 2) || (showAddressError == 2 && showDistanceError == 0) || (showAddressError == 2 && showDistanceError == 2)) {
        // if there are no real errors but is still undefined.
        $("#error-container").addClass("invisible");
        $("#submit-button").attr("disabled", "disabled");

    } else if (!showAddressError && !showDistanceError) {
        // Form validated successfully.
        // Hide the error messages.
        $("#error-container").addClass("invisible");

        // Enable the submit button.
        $("#submit-button").removeAttr('disabled');
    }
}

// Check that the postcode is of valid format
function isValidPostcode(p) {
    var postcodeRegEx = /[A-Z]{1,2}[0-9]{1,2} ?[0-9][A-Z]{2}/i;
    return postcodeRegEx.test(p);
}

// Format the postcode to make it looks nice.
function formatPostcode(p) {
    if (isValidPostcode(p)) {
        var postcodeRegEx = /(^[A-Z]{1,2}[0-9]{1,2})([0-9][A-Z]{2}$)/i;
        return p.replace(postcodeRegEx, "$1 $2");
    } else {
        return p;
    }
}

// Only allow numeric input.
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }

    return true;
}