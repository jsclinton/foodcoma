﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using FoodComa.BLL;
using FoodComa.Models;

namespace FoodComa.Controllers
{
    public class IndexController : BaseController
    {
        //
        // GET: /Index/

        public ActionResult Index()
        {
            ViewBag.Title = "| Get me food!";
            return View();
        }

        [HttpPost]
        public ActionResult Index(string address, double distance, string latitude, string longitude, bool useGps = false)
        {
            var coordinates = new FoodComa.Models.Coordinates(longitude, latitude);
            var userQuery = new FoodComa.Models.Query(address, distance, useGps, coordinates);
            TempData["userQuery"] = userQuery;
            TempData.Keep("userQuery");
            return RedirectToAction("Show", "Venue");
        }

        public ActionResult ReturnToIndex()
        {
            var message = (string)TempData["message"];
            var query = (FoodComa.Models.Query)TempData["query"];
            ViewBag.Title = "| Get me food!";
            var error = new Error(message, query);
            return View("Index", error);
        }
    }
}
