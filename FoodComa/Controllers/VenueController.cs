﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FoodComa.BLL;
using FoodComa.Models;
using FoodComa.Misc;

namespace FoodComa.Controllers
{
    public class VenueController : BaseController
    {
        private List<string> priceList = new List<string>
            {
                "Free",
                "Inexpensive",
                "Moderate",
                "Expensive",
                "Very Expensive"
            };

        public VenueController()
        {
        }

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Show()
        {
            var query = (FoodComa.Models.Query) TempData["userQuery"];

            var businessLogic = new BusinessLogic();
            var venueCount = new int();

            var venue = new FoodComa.BLL.Venue();

            var businessCoordinates = new FoodComa.BLL.Coordinates(query.Coordinates.Longitude,
                                                                   query.Coordinates.Latitude);
            var businessQuery = new FoodComa.BLL.Query(query.QueryAddress, query.Distance, query.UseGps,
                                                       businessCoordinates);

            if (businessQuery.UseGps)
            {
                venue = businessLogic.GetRestaurant(businessQuery, businessQuery.Coordinates, ref venueCount);
            }
            else
            {
                if (query.QueryAddress == "")
                {
                    return ReturnToIndex(ErrorCodes.NoAddressSpecified, query);
                }
                else
                {
                    venue = businessLogic.GetRestaurant(businessQuery,
                                                        businessLogic.GetCoordinates(businessQuery.QueryAddress),
                                                        ref venueCount);
                }
            }

            if (venue == null)
            {
                return ReturnToIndex(ErrorCodes.NoRestaurantsFound, query);
            }

            // Check if any of the opening times are split.
            var splitTimes = venue.OpeningTimes.Select(x => x.DinnerHours).ToList();

            var modelVenue = new Models.Venue
                {
                    Name = venue.Name,
                    Address = venue.Address,
                    Rating = venue.Rating,
                    Geolocation = new FoodComa.Models.Coordinates(venue.Longitude, venue.Latitude),
                    OpenNow = venue.OpenNow,
                    PostCode = venue.PostCode,
                    Website = venue.Website,
                    PhoneNumber = venue.PhoneNumber,
                    OtherVenues = venueCount,
                    Price = priceList[venue.Price],
                    OpeningTimes = new List<OpeningHours>(
                        venue.OpeningTimes.Select(x =>
                                                  new Models.OpeningHours
                                                      {
                                                          Day = x.Day,
                                                          DinnerHours = x.DinnerHours,
                                                          LunchHours = x.LunchHours
                                                      }).ToArray()
                        ),
                    Reviews = new List<GoogleReview>(
                        venue.Reviews.Select(x =>
                                             new Models.GoogleReview(x.Name, x.Text, x.Rating))),
                    Photo = venue.Photo
                };

            ViewBag.Title = "| Food has been decided!";

            ViewData["Query"] = query;
            TempData["userQuery"] = query;

            if (modelVenue.Photo != null)
            {
                // Convert the image to a byte array.
                var memoryStream = new MemoryStream();
                modelVenue.Photo.Save(memoryStream, ImageFormat.Png);

                var data = memoryStream.ToArray();
                var imageBase64 = Convert.ToBase64String(data);
                var imageSrc = string.Format("data:image/gif;base64,{0}", imageBase64);

                TempData["photo"] = imageSrc;
            }
            return View(modelVenue);
        }

        public ActionResult ReturnToIndex(string errorMessage, FoodComa.Models.Query query)
        {
            TempData["query"] = query;
            TempData["message"] = errorMessage;
            return RedirectToAction("ReturnToIndex", "Index");
        }
    }
}