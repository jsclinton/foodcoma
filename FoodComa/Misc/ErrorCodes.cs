﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.Misc
{
    public static class ErrorCodes
    {
        public const string NoRestaurantsFound = "No restaurants found in the area, try increasing the distance.";
        public const string NoAddressSpecified = "Address was not specified.";
    }
}