﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Web;
using FoodComa.BLL;

namespace FoodComa.Models
{
    public class Venue
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Rating { get; set; }
        public Coordinates Geolocation { get; set; }
        public string OpenNow { get; set; }
        public string PhoneNumber { get; set; }
        public string Website { get; set; }
        public List<OpeningHours> OpeningTimes { get; set; }
        public string PostCode { get; set; }
        public int OtherVenues { get; set; }
        public string Price { get; set; }
        public List<GoogleReview> Reviews { get; set; }
        public Image Photo { get; set; }
    }
}