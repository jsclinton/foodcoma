﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.Models
{
    public class Coordinates
    {
        /// <summary>
        /// East / West.
        /// </summary>
        public string Longitude { get; set; }

        /// <summary>
        /// North / South.
        /// </summary>
        public string Latitude { get; set; }

        public Coordinates(string longitude, string latitude)
        {
            this.Longitude = longitude;
            this.Latitude = latitude;
        }
    }
}