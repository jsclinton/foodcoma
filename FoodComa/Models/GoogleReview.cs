﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FoodComa.Models
{
    public class GoogleReview
    {
        public string Name { get; set; }
        public string Text { get; set; }
        public string Rating { get; set; }

        public GoogleReview()
        {
            
        }

        public GoogleReview(string name, string text, string rating)
        {
            this.Name = name;
            this.Text = text;
            this.Rating = rating;
        }
    }
}