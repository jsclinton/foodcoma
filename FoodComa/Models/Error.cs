﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FoodComa.BLL;

namespace FoodComa.Models
{
    public class Error
    {
        public string Message { get; set; }
        public Query Query { get; set; }

        public Error(string message, Query query)
        {
            this.Message = message;
            this.Query = query;
        }
    }
}